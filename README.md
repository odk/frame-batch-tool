# Frame Batch Tool

Python CLI for sending a batch of frames to the ODK API for analyses.

## Requirements

- [requests](https://requests.readthedocs.io)

## Usage

Help:
```
$ python main.py --help
usage: main.py [-h] [-a API_URL] [-u API_USERNAME] [-p] [-i IMG_FOLDER]

Tool for sending batch of frames to ODK API

optional arguments:
  -h, --help            show this help message and exit
  -a API_URL, --api API_URL
                        ODK API URL
  -u API_USERNAME, --username API_USERNAME
                        ODK API username
  -p, --password        Keep empty, will prompt for ODK API password
  -i IMG_FOLDER, --images IMG_FOLDER
                        Folder with scan images
```

Example:
```
$ python main.py --api http://192.168.0.100:8090 --images ~/odk-frames --username admin -p
Password: ***
```


## Dependencies for testing

- Frames: check datetime + location formatting, may differ
- Locally running [ODK API + stack](https://gitlab.com/odk/odk-stack) (PostgreSQL, RabbitMQ)
- Locally running [frame analyser](https://gitlab.com/odk/odk-frame-analyzer), listining to local RabbitMQ
