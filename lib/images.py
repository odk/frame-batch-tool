from typing import Union, List
from pathlib import Path, PosixPath
from datetime import datetime
import base64

from lib.frame import RawFrame, RawFrameUpdated


def get_files(path) -> List[PosixPath]:
    jpg_folder = Path(path).glob("**/*.jpg")
    jpg_files = [x for x in jpg_folder]
    return jpg_files


def create_base64(file) -> str:
    with open(str(file), "rb") as img_file:
        base64_bytes = base64.b64encode(img_file.read())
        base64_str = base64_bytes.decode("utf-8")
        return base64_str


def translate_date_string(date_str) -> str:
    date_fmt = "%Y-%m-%d"
    time_dots = "%H.%M.%S.%f"
    time_colon_dot = "%H:%M:%S.%f"
    accepted_input_formats = (
        f"{date_fmt} {time_dots}",
        f"{date_fmt} {time_colon_dot}",
        f"{date_fmt}T{time_dots}",
        f"{date_fmt}T{time_colon_dot}"
    )
    output_format = "%Y-%m-%d %H:%M:%S.%f"

    for fmt in accepted_input_formats:
        try:
            date_obj = datetime.strptime(date_str, fmt)
            return datetime.strftime(date_obj, output_format)
        except ValueError:
            pass
    raise ValueError(f"Invalid date format: {date_str}")


def init_raw_frame(file_name, base64_str, sub_folder_name = None) -> Union[RawFrame, None]:
    try:
        split_string = file_name.split("_")
        frame_date_str = split_string[0]
        frame_date_str = translate_date_string(frame_date_str)

        frame_lat: float = split_string[1]
        frame_lng: float = split_string[2]

        if sub_folder_name:
            stream_id = sub_folder_name
        else:
            stream_id = "batch_tool"

        # rf = RawFrame(
        #     img=base64_str,
        #     lng=frame_lng,
        #     lat=frame_lat,
        #     timestamp=frame_date_str,
        #     stream_id=stream_id
        # )

        rf = RawFrameUpdated(
            img=base64_str,
            lng=frame_lng,
            lat=frame_lat,
            timestamp=frame_date_str,
            stream_id=stream_id,
            stream_meta={
                "velocity": 2,
                "user_type": "batch_tool"
            }
        )

        return rf
        
    except ValueError as e:
        print(e)
        return None

    except Exception as e:
        print(e)
        return None

