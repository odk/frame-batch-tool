from dataclasses import dataclass

@dataclass
class RawFrame:
    img: str
    lng: float
    lat: float
    timestamp: str
    stream_id: str = "batch_tool"
    user_type: str = "batch_tool"
    vehicle_type: str = "batch_tool"
    user_id: str = "batch_tool"

    def name(self) -> str:
        return f"{self.timestamp}_{self.lat}_{self.lng}"


@dataclass
class RawFrameUpdated:
    img: str
    lng: float
    lat: float
    timestamp: str
    stream_meta: dict
    stream_id: str = "batch_tool"

    def name(self) -> str:
        return f"{self.timestamp}_{self.lat}_{self.lng}"