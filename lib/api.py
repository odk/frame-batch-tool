import json
from dataclasses import asdict
from urllib.parse import urljoin

import requests

from lib.frame import RawFrame


class API:
    def __init__(self, api_url: str) -> None:
        self.API_URL = api_url
        self.FRAME_ENDPOINT = "raw_frame"
        self.LOGIN_ENDPOINT = "login"
        self.ACCESS_TOKEN = None


    def login(self, username: str, password: str) -> str:
        url = urljoin(self.API_URL, self.LOGIN_ENDPOINT)
        access_token = None
        success = None
        try:
            # FIX: do not add 'multipart/form-data' header!
            r = requests.post(
                url=url,
                files={
                    "username": (None, username),
                    "password": (None, password)
                }
            )

            # print(r.status_code, r.reason, r.json())
            access_token = r.json().get("access_token")
            success = True

        except Exception as e:
            print("Login failed!", e)
            success = False
        finally:
            self.ACCESS_TOKEN = access_token
            return success


    def send_frame(self, raw_frame: RawFrame) -> None:
        url = urljoin(self.API_URL, self.FRAME_ENDPOINT)
        headers = {
            "content-type": "application/json",
            "Authorization": f"Bearer {self.ACCESS_TOKEN}"
        }

        try:
            r = requests.post(
                url=url,
                headers=headers,
                data=json.dumps(asdict(raw_frame)),
            )

            # print(r.status_code, r.reason, r.text)
            print(raw_frame.name(), r.reason)
            
        except Exception as e:
            raise e
