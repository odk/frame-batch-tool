import sys
import argparse
import signal
from getpass import getpass
from lib.api import API
from lib.images import get_files, create_base64, init_raw_frame

DEFAULT_API_URL = "http://localhost:8090"
DEFAULT_API_USERNAME = "admin"
DEFAULT_API_PASSWORD = "admin"
DEFAULT_IMG_FOLDER = "./"
DEFAULT_INCL_FOLDER = False


def main():
    parser = argparse.ArgumentParser(
        description="Tool for sending batch of frames to ODK API"
    )
    parser.add_argument("-a", "--api", dest="api_url", help="ODK API URL", default=DEFAULT_API_URL)
    parser.add_argument("-u", "--username", dest="api_username", help="ODK API username", default=DEFAULT_API_URL)
    parser.add_argument("-p", "--password", action="store_true", dest="api_password", help="Keep empty, will prompt for ODK API password")
    parser.add_argument("-i", "--images", dest="img_folder", help="Folder with scan images", default=DEFAULT_IMG_FOLDER)
    parser.add_argument("-f", "--include-folder", action="store_true", dest="include_folder", help="Includes subfolder name as stream id", default=DEFAULT_INCL_FOLDER)


    args = parser.parse_args()
    api_url = args.api_url
    username = args.api_username
    img_folder = args.img_folder
    include_folder = args.include_folder

    print("API URL set as:", api_url)
    print("Looking for images in:", img_folder)

    api = API(api_url)

    if args.api_password:
        password = getpass()
    else:
        password = DEFAULT_API_PASSWORD

    success = api.login(username, password)
    if not success:
        sys.exit()

    files = get_files(img_folder)
    if not files:
        print("No images found in selected folder")
        sys.exit()

    for file in files:
        base64_str = create_base64(file)
        if include_folder:
            sub_folder_name = file.parts[len(file.parts)-2]
        else:
            sub_folder_name = None
        rf = init_raw_frame(file.stem, base64_str, sub_folder_name)
        if rf:
            api.send_frame(rf)


if __name__ == "__main__":
    def signal_handler(signal, frame):
        print("\nExiting...")
        sys.exit()

    signal.signal(signal.SIGINT, signal_handler)

    main()